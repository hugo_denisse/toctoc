const serverConfig = require("./config/server");
const Koa = require("koa");
const server = require("./server");
const dbConfig = require("./config/database");

const app = new Koa();
server(app);
dbConfig();
app.listen(serverConfig.port);
console.log(`server started, listening on port ${serverConfig.port}`);
