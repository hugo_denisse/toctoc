const Router = require("koa-router");
const jwt = require("koa-jwt");
const bodyParser = require("koa-bodyparser");
const { secret } = require("../config/jwt");

const errorHandler = require("../middlewares/errorHandler");
const auth = require("./auth");
const ping = require("./ping");
const test = require("./test");

const publicRouter = new Router();
ping(publicRouter);
auth(publicRouter);

const privateRouter = new Router();
privateRouter.use(jwt({ secret }));
test(privateRouter);

module.exports = function(app) {
  app.use(errorHandler);
  app.use(bodyParser());
  app.use(publicRouter.routes());
  app.use(privateRouter.routes());
};
