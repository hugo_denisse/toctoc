const controller = require("./auth.controller");

module.exports = function(router) {
  router.post("/password", controller.password);
  router.post("/register", controller.register);
  router.post("/refresh", controller.refresh);
  router.post("/logout", controller.logout);
};
