const jwt = require("jsonwebtoken");
const Joi = require("joi");
const uuid = require("uuid/v4");
const crypto = require("crypto");
const co = require("co");

const { secret } = require("../../config/jwt");

const joiSchemas = require("./auth.schema");
const UserModel = require("../../models/user.model");
const TokenModel = require("../../models/token.model");
const jwtConfig = require("../../config/jwt");

function createAccessToken(user) {
  return jwt.sign(
    {
      uuid: user.uuid,
      name: user.name,
      role: user.role,
      email: user.email
    },
    secret,
    {
      expiresIn: jwtConfig.access_expiration
    }
  );
}

function hashPassword(password) {
  const salt = crypto.randomBytes(16).toString("hex");
  const hash = crypto
    .pbkdf2Sync(password, salt, 2048, 32, "sha512")
    .toString("hex");
  return [salt, hash].join("$");
}

function verifyHash(password, original) {
  const originalHash = original.split("$")[1];
  const salt = original.split("$")[0];
  const hash = crypto
    .pbkdf2Sync(password, salt, 2048, 32, "sha512")
    .toString("hex");
  return hash === originalHash;
}

/**
 * Inscription d'un utilisateur
 * @param {*} ctx
 */
async function register(ctx) {
  return co(function*() {
    const requestData = Joi.attempt(
      ctx.request.body,
      joiSchemas.registerSchema
    );
    console.log("input /register");

    const exist = yield UserModel.findOne({
      $or: [{ email: requestData.email }, { name: requestData.name }]
    }).exec();

    if (exist) {
      console.error("user with this name/email already exist");
      throw {
        status: 409,
        message: "Name or email already taken"
      };
    }
    const hashedPassword = hashPassword(requestData.password);
    const user = new UserModel({
      uuid: uuid(),
      name: requestData.name,
      email: requestData.email,
      password: hashedPassword,
      created_from: requestData.from
    });
    yield user.save();

    console.log({ user }, "user created successfully");

    const access_token = createAccessToken(user),
      refresh_token = uuid();

    ctx.body = {
      success: true,
      access_token,
      refresh_token
    };

    const saveToken = new TokenModel({
      token: refresh_token,
      user_uuid: user.uuid,
      from: requestData.from
    });
    yield saveToken.save();
  });
}

/**
 * Connexion via email/mot de passe
 * @param {*} ctx
 */
async function password(ctx) {
  return co(function*() {
    const requestData = Joi.attempt(
      ctx.request.body,
      joiSchemas.passwordSchema
    );
    console.log("input /password");

    const user = yield UserModel.findOne({
      email: requestData.email
    }).exec();

    if (!user) {
      console.log("user not found");
      throw {
        status: 401,
        message: "Invalid credentials"
      };
    }

    if (!verifyHash(requestData.password, user.password)) {
      console.log("user found but password didn't match");
      throw {
        status: 401,
        message: "Invalid credentials"
      };
    }

    console.log({ user }, "user found");

    const access_token = createAccessToken(user),
      refresh_token = uuid();

    ctx.body = {
      success: true,
      access_token,
      refresh_token
    };

    const saveToken = new TokenModel({
      token: refresh_token,
      user_uuid: user.uuid,
      from: requestData.from
    });
    yield saveToken.save();
  });
}

/**
 * Connexion via refresh_token
 * @param {*} ctx
 */
async function refresh(ctx) {
  return co(function*() {
    const requestData = Joi.attempt(ctx.request.body, joiSchemas.refreshSchema);
    console.log("input /refresh");

    // expiration infinie pour l'instant
    const token = yield TokenModel.findOne({
      token: requestData.token,
      from: requestData.from
    }).exec();

    if (!token) {
      throw {
        status: 401,
        message: "Invalid refresh token"
      };
    }

    const user = yield UserModel.findOne({ uuid: token.user_uuid }).exec();

    if (!user) {
      throw {
        status: 401,
        message: "User not found"
      };
    }

    console.log({ user }, "user found");

    const access_token = createAccessToken(user);

    ctx.body = {
      success: true,
      access_token
    };
  });
}

/**
 * Invalide un refresh token
 * @param {*} ctx
 */
async function logout(ctx) {
  return co(function*() {
    const requestData = Joi.attempt(ctx.request.body, joiSchemas.logoutSchema);
    console.log("input /logout");

    yield TokenModel.findOne({
      token: requestData.token
    })
      .remove()
      .exec();

    ctx.body = {
      success: true
    };
  });
}

module.exports = {
  password,
  register,
  refresh,
  logout
};
