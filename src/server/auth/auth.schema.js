const Joi = require("joi");

const passwordSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string().required(),
  from: Joi.string()
});

const registerSchema = Joi.object().keys({
  name: Joi.string()
    .alphanum()
    .min(3)
    .max(20)
    .required(),
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string()
    .required()
    .min(6),
  from: Joi.string()
});

const refreshSchema = Joi.object().keys({
  token: Joi.string().required(),
  from: Joi.string()
});

const logoutSchema = Joi.object().keys({
  token: Joi.string().required(),
  from: Joi.string()
});

module.exports = {
  passwordSchema,
  registerSchema,
  refreshSchema,
  logoutSchema
};
