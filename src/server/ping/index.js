const controller = require("./ping.controller");

module.exports = function(router) {
  router.get("/ping", controller.ping);
};
