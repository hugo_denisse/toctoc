module.exports = {
  test: ctx => {
    ctx.response.body = {
      success: true,
      message: `Hello ${ctx.state.user.name}! Role: ${ctx.state.user.role}.`
    };
  }
};
