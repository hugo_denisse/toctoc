const controller = require("./test.controller");

module.exports = function(router) {
  router.get("/test", controller.test);
};
