module.exports = {
  secret: process.env.JWT_SECRET || "test",
  access_expiration: process.env.JWT_ACCESS_EXP || 86400
};
