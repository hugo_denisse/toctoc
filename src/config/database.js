const mongoose = require("mongoose");

const dbname = process.env.DB_NAME || "toctoc";

const config = () => {
  mongoose
    .connect(`mongodb://localhost/${dbname}`, {
      keepAlive: 120,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000
    })
    .then(() => {
      console.log("connected to db successfully");
    })
    .catch(err => {
      console.error("could not connect to db", { err });
    });
};

module.exports = config;
