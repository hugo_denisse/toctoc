const mongoose = require("mongoose");

module.exports = mongoose.model("User", {
  uuid: String,
  name: String,
  email: String,
  password: String,
  created_at: { type: Date, default: Date.now },
  created_from: String,
  role: { type: String, default: "user" }
});
