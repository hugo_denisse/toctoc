const mongoose = require("mongoose");

module.exports = mongoose.model("Token", {
  token: String,
  user_uuid: String,
  from: String,
  created_at: { type: Date, default: Date.now }
});
