module.exports = (ctx, next) => {
  return next().catch(err => {
    console.error("handle err", { err });
    ctx.status = err.status || 500;

    let message = err.originalError ? err.originalError.message : err.message;
    let details;
    if (err.isJoi) {
      ctx.status = 400;
      details = {};
      err.details.forEach(detail => {
        details[detail.context.key] = [detail.message];
      });
    } else if (err.status === undefined && err.name === "JsonWebTokenError") {
      ctx.status = 401;
    }

    ctx.body = {
      success: false,
      error: err.name,
      message,
      details
    };
  });
};
