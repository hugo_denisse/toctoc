# Toctoc

This microservice is responsible for authentication and registration of users.

## Installation

Requirements: Node and Mongodb

```sh
git clone https://bitbucket.org/hugo_denisse/toctoc
cd toctoc
npm install
```

To launch the development server, type:

```sh
npm start
```

## Environment variables

| Name           | Description                         | Default value |
| -------------- | ----------------------------------- | ------------- |
| DB_NAME        | name of the mongo database          | toctoc        |
| PORT           | port on which is running the server | 8080          |
| JWT_SECRET     | secret used to encode JWT           | test          |
| JWT_ACCESS_EXP | expiration time for access tokens   | 86400         |
